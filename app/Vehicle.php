<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';
}
