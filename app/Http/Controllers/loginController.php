<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{
  public function authenticate(Request $request)
 {
   $email = $request->input('email');
   $password = $request->input('password');
     if (Auth::attempt(['email' => $email, 'password' => $password])) {
       return  response()->json([
          'message' => 'Login success'
      ]);
     }
     else{
       return  response()->json([
            'message' => 'Login failure'
        ]);
     }
 }
}
