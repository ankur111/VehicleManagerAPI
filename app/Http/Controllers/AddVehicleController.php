<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Vehicle;
use App\Http\Controllers\Controller;

class AddVehicleController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request)
 {
   //validate the Request
   $vehicle = new Vehicle;

   $vehicle->email            = $request->email;
   $vehicle->vehicle_type     = $request->vehicletype;
   $vehicle->manufacturer     = $request->manufacturer;
   $vehicle->reg_number       = $request->reg;
   $vehicle->capacity         = $request->capacity;

   $vehicle->save();
 }
}
