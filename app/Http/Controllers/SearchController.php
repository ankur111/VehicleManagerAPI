<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $vehicleType  =   $request->input('Vehicle');
      $reqFromdate  =   $request->input('DateFrom');
      $reqTodate    =   $request->input('DateTo');

      $vehicles = DB::table('vehicles')->get();

      return  response()->json([
        'vehicles' => $vehicles,
     ]);
    }
}
